import React, { useState, useEffect } from "react";

import Search from "./components/Search";
import Main from "./components/Main";
import Footer from "./components/footer";

import storm from "./img/weather-icons/storm.svg";
import clear from "./img/weather-icons/clear.svg";
import drizzle from "./img/weather-icons/drizzle.svg";
import fog from "./img/weather-icons/fog.svg";
import mostlycloudy from "./img/weather-icons/mostlycloudy.svg";
import partlycloudy from "./img/weather-icons/partlycloudy.svg";
import rain from "./img/weather-icons/rain.svg";
import snow from "./img/weather-icons/snow.svg";
import "./App.css";

const App = () => {
  const [city, setCity] = useState("");
  const [data, setData] = useState();

  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch(
        `http://api.openweathermap.org/data/2.5/forecast?q=${city}&cnt=8&units=metric&appid=ea78bf92214df28c3ed344ca5602cc25`
      );
      const data = await res.json();
      setData(data);
      console.log(data);
    };
    fetchData();
  }, [city]);

  const findWeather = () => {
    setCity(document.querySelector("input").value);

  };
  const liveImages = (id) => {
    if (id <= 300) {
      document.body.style.background = "rgb(144, 209, 221)";
      document.body.style.transition = "background 2s";
      document.querySelector(".header").style.background = "rgb(90, 90, 248)";
      document.querySelector(".header").style.transition = "background 2s";
      return storm;
    } else if (id > 300 && id <= 499) {
      document.body.style.background = "green-blue";
      document.body.style.transition = "background 2s";
      document.querySelector(".header").style.background = "rgb(90, 90, 248)";
      document.querySelector(".header").style.transition = "background 2s";
      return drizzle;
    } else if (id >= 500 && id <= 599) {
      document.body.style.background = "rgb(144, 209, 221)";
      document.body.style.transition = "background 2s";
      document.querySelector(".header").style.background = "rgb(90, 90, 248)";
      document.querySelector(".header").style.transition = "background 2s";
      return rain;
    } else if (id >= 600 && id <= 699) {
      document.body.style.background = "rgb(144, 209, 221)";
      document.body.style.transition = "background 2s";
      document.querySelector(".header").style.background = "rgb(90, 90, 248)";
      document.querySelector(".header").style.transition = "background 2s";
      return snow;
    } else if (id >= 700 && id <= 799) {
      document.body.style.background = "rgb(144, 209, 221)";
      document.body.style.transition = "background 2s";
      document.querySelector(".header").style.background = "rgb(90, 90, 248)";
      document.querySelector(".header").style.transition = "background 2s";
      return fog;
    } else if (id == 800) {
      document.body.style.background = "rgb(144, 209, 221)";
      document.body.style.transition = "background 2s";
      document.querySelector(".header").style.background = "rgb(90, 90, 248)";
      document.querySelector(".header").style.transition = "background 2s";
      return clear;
    } else if (id == 801) {
      document.body.style.background = "rgb(144, 209, 221)";
      document.body.style.transition = "background 2s";
      document.querySelector(".header").style.background = "rgb(90, 90, 248)";
      document.querySelector(".header").style.transition = "background 2s";
      return partlycloudy;
    } else if (id > 801 && id <= 805) {
      document.body.style.background = "blue";
      document.body.style.transition = "background 2s";
      document.querySelector(".header").style.background = "rgb(90, 90, 248)";
      document.querySelector(".header").style.transition = "background 2s";
      return mostlycloudy;
    }
  };

  return (
    <div>
      <Search findWeather={findWeather} />
      {data && data.list && (
        <>
          <Main
            newImages={liveImages(data.list[0].weather[0].id)}
            humid={data.list[0].main.humidity}
            press={data.list[0].main.pressure}
            description={data.list[0].main.description}
            minTemp={data.list[0].main.temp_min}
            maxTemp={data.list[0].main.temp_max}
          />
          <Footer hoursMain={data.list.slice(1)} />
        </>
      )}
    </div>
  );
};
export default App;






// import React, { useState, useEffect } from "react";
// import Search from "./components/Search";
// import Main from "./components/Main";
// import Footer from "./components/footer";
// import FakeWeather from "./fakeWeatherData.json";
// //  import TheWeatherNow from "./components/TheWeatherNow";
// // import  WeatherOver24h from "./components/WeatherOver24h ";
// import storm from "../img/weather-icons/storm.svg";
// import clear from "../img/weather-icons/clear.svg";
// import drizzle from "../img/weather-icons/drizzle.svg";
// import fog from "../img/weather-icons/fog.svg";
// import mostlycloudy from "../img/weather-icons/mostlycloudy.svg";
// import partlycloudy from "../img/weather-icons/partlycloudy.svg";
// import rain from "../img/weather-icons/rain.svg";
// import snow from "../img/weather-icons/snow.svg";
// import "./App.css";
// const App = () => {


//   const [city, setCity] = useState("");
//   const [data, setData] = useState();

//   useEffect(() => {
//     const fetchData = async () => {
//       const res = await fetch(
//         `http://api.openweathermap.org/data/2.5/forecast?q=${city}&cnt=8&units=metric&appid=ae4834affa839a9f578212d5b1742a20`
//       );
//       const data = await res.json();
//       setData(data);
//       console.log(data);
//     };
//     fetchData();
//   }, [city]);

//   const findWeather = () => {
//     setCity(document.querySelector("input").value);
//     // console.log(document.querySelector("input").value);
//   };
//   const hdt = [];

//   // for (let i = 1; i <= 7; i++) {
//   //   hdt.push(FakeWeather.list[i]);
//   // }
//   const liveImages = (id) => {
//     if (id <= 300) {
//       document.body.style.background = "gold";
//       document.body.style.transition = "background 2s";
//       document.querySelector(".nav").style.background = "grey";
//       document.querySelector(".nav").style.transition = "background 2s";
//       return storm;
//     } else if (id > 300 && id <= 499) {
//       document.body.style.background = "green-blue";
//       document.body.style.transition = "background 2s";
//       document.querySelector(".nav").style.background = "green";
//       document.querySelector(".nav").style.transition = "background 2s";
//       return drizzle;
//     } else if (id >= 500 && id <= 599) {
//       document.body.style.background = "white";
//       document.body.style.transition = "background 2s";
//       document.querySelector(".nav").style.background = "white";
//       document.querySelector(".nav").style.transition = "background 2s";
//       return rain;
//     } else if (id >= 600 && id <= 699) {
//       document.body.style.background = "grey";
//       document.body.style.transition = "background 2s";
//       document.querySelector(".nav").style.background = "grey";
//       document.querySelector(".nav").style.transition = "background 2s";
//       return snow;
//     } else if (id >= 700 && id <= 799) {
//       document.body.style.background = "blue";
//       document.body.style.transition = "background 2s";
//       document.querySelector(".nav").style.background = "grey";
//       document.querySelector(".nav").style.transition = "background 2s";
//       return fog;
//     } else if (id == 800) {
//       document.body.style.background = "blue";
//       document.body.style.transition = "background 2s";
//       document.querySelector(".nav").style.background = "grey";
//       document.querySelector(".nav").style.transition = "background 2s";
//       return clear;
//     } else if (id == 801) {
//       document.body.style.background = "blue";
//       document.body.style.transition = "background 2s";
//       document.querySelector(".nav").style.background = "black";
//       document.querySelector(".nav").style.transition = "background 2s";
//       return partlycloudy;
//     } else if (id > 801 && id <= 805) {
//       document.body.style.background = "blue";
//       document.body.style.transition = "background 2s";
//       document.querySelector(".nav").style.background = "black";
//       document.querySelector(".nav").style.transition = "background 2s";
//       return mostlycloudy;
//     }
//   };
//   return (
//     <div >
//       <Search findWeather={findWeather} />
//       {data && data.list && (
//         <>
//           <Main
//             //iconSrc={icon(data.list[0].weather[0].id, 1)}
//             newImages={liveImages(data.list[0].weather[0].id)}
//             humid={data.list[0].main.humidity}
//             press={data.list[0].main.pressure}
//             description={data.list[0].main.description}
//             minTemp={data.list[0].main.temp_min}
//             maxTemp={data.list[0].main.temp_max}
//           />
//           <Footer hoursFooter={data.list.slice(1)} />
//         </>   
//       )}
//     </div>
//   );
// };
// export default App;




// // const icon = (id , flag =>{
// //   if(id < 300){
// //     if (flag){
// //       document.body.style.background="#747880";
// //       document.querySelector("#header").style.background;
// //       document.body.style.transition = "background 2s";
// //     }
// //   }
// // })

// // const liveImage = (id) => {
//   //   if (id < 300) { return storm }
//   //   else if (id > 300 && id <= 499) { return drizzle }
//   //   else if (id >= 500 && id <= 599) { return rain }
//   //   else if (id >= 600 && id <= 699) { return snow }
//   //   else if (id >= 700 && id < 799) { return fog }
//   //   else if (id = 800) { return clear }
//   //   else if (id = 801) { return partlycloudy }
//   //   else if (id >= 801 && id <= 805) { return mostlycloudy }
//   // };















// // import React, { useEffect, useState } from "react";
// // import Search from "./components/Search";
// // import Main from "./components/Main";
// // import WeatherItem from "./components/WeatherItem";
// // import fakeWeather from "./fakeWeatherData.json";
// // import "./App.css";
// // import Search from "/home/ibrahim/Desktop/weather-api/src/app.env";
// // // import SayHi, { SayHello } from "./components/WeatherItem";
// // // import fakeWeatherData from "./fakeWeatherData.json";

// // // REACT_APP_API_URL = 'https://api.openweathermap.org/data/2.5'
// // // REACT_APP_API_KEY = ' ae4834affa839a9f578212d5b1742a20'
// // // REACT_APP_ICON_URL = 'https://openweathermap.org/img/w'


// // const App = () => {
// //   const [lat, setLat] = useState([]);
// //   const [long, setLong] = useState([]);

// //   //ae4834affa839a9f578212d5b1742a20
// //   useEffect(() => {
// //     navigator.geolocation.getCurrentPosition(function(position) {
// //       setLat(position.coords.latitude);
// //       setLong(position.coords.longitude);
// //     });

// //     console.log("Latitude is:", lat)
// //     console.log("Longitude is:", long)
// //   }, [lat, long]);

// //   return (
// //     <div id="love">
// //     <Search />


// //       <Main humd={fakeWeather.list[0].main.humidity}
// //        pres={fakeWeather.list[0].main.pressure}
// //        temp ={fakeWeather.list[0].main.humidity} 
// //   //     temp-min={fakeWeather.list[0].main.humidity} 
// //   // temp-max={fakeWeather.list[0].main.humidity} 
// //       />


// //       <WeatherItem 
// //       // degree3={((fakeWeather.list[1].main.temp)-273.15).toFixed(1)}
// //       // degree6={((fakeWeather.list[2].main.temp)-273.15).toFixed(1)}
// //       // degree9={((fakeWeather.list[3].main.temp)-273.15).toFixed(1)}
// //       // degree12={((fakeWeather.list[4].main.temp)-273.15).toFixed(1)}
// //       // degree15={((fakeWeather.list[5].main.temp)-273.15).toFixed(1)}
// //       // degree18={((fakeWeather.list[6].main.temp)-273.15).toFixed(1)}
// //       // degree21={((fakeWeather.list[7].main.temp)-273.15).toFixed(1)}
// //       times=
// //       />

// //     </div>

// //   );
// // };

// // export default App;
