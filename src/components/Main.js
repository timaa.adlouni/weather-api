// import React from "react";
// import imgmain from "../img/weather-icons/snow.svg";

// const Main = (props) => {
//     return (
//         <main id="clear">
//             <div >
//                 <img src={props.newImages} alt="storm icon" width="300px" />
//             </div>

//             <div>{props.description}</div>

//             <div className="temperature">
//                 <span>Temperture </span>
//                 {Math.floor(props.minTemp)}°C to{" "}
//                 {Math.floor(props.maxTemp)} °C
//             </div>

//             <div className="hum-pres">
//                 <span>humidty {props.humid}</span> <span>pressure {props.press}</span>
//             </div>
//         </main>
//     );
// };
// export default Main;


import React from "react";
import imgmain from "../img/weather-icons/snow.svg";

const Main = (props) => {
  return (
    <main id="clear">
      <div className="picture">
        <img src={props.newImages} alt="storm icon" />
      </div>

      <div className="clouds">{props.description}</div>

      <div className="temperature">
        <span>Temperture </span>
        {Math.floor(props.minTemp)}°C to {Math.floor(props.maxTemp)} °C
      </div>

      <div className="hum-pres">
        <span>humidty {props.humid}</span> <span>pressure {props.press}</span>
      </div>
    </main>
  );
};
export default Main;
// http://api.openweathermap.org/data/2.5/forecast?q=${CITY_NAME}&cnt=8&units=metric&appid=ea78bf92214df28c3ed344ca5602cc25

