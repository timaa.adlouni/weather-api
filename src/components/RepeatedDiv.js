// import React from "react";
// import img1 from "../img/weather-icons/clear.svg";

// const RepeatedDiv = (props) => {
//   return (
//     <div className="final-section">
//       <div className="time1">{props.time1}:00</div>
//       <div className="image image1">
//         <img src={props.src} alt="storm icon" />
//       </div>
//       <div className="degree1">{Math.floor(props.degree1 )}°C</div>
//     </div>
//   );
// };
// export default RepeatedDiv;


import React from "react";
import img1 from "../img/weather-icons/clear.svg";

const RepeatedDiv = (props) => {
  return (
    <div className="final-section">
      <div className="time1">{props.time1}:00</div>
      <div className="image image1">
        <img src={props.src} alt="storm icon" />
      </div>
      <div className="degree1">{Math.floor(props.degree1 )}°C</div>
    </div>
  );
};
export default RepeatedDiv;