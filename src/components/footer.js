import React from "react";
import RepeatedDiv from "./RepeatedDiv";
import FakeWeather from "../fakeWeatherData.json";

import storm from "../img/weather-icons/storm.svg";
import clear from "../img/weather-icons/clear.svg";
import drizzle from "../img/weather-icons/drizzle.svg";
import fog from "../img/weather-icons/fog.svg";
import mostlycloudy from "../img/weather-icons/mostlycloudy.svg";
import partlycloudy from "../img/weather-icons/partlycloudy.svg";
import rain from "../img/weather-icons/rain.svg";
import snow from "../img/weather-icons/snow.svg";

const Footer = ({ hoursMain }) => {
  const liveImages = (id, flag) => {
    if (id <= 300) {
      return storm;
    } else if (id > 300 && id <= 499) {
      return drizzle;
    } else if (id >= 500 && id <= 599) {
      return rain;
    } else if (id >= 600 && id <= 699) {
      return snow;
    } else if (id >= 700 && id <= 799) {
      return fog;
    } else if (id == 800) {
      return clear;
    } else if (id == 801) {
      return partlycloudy;
    } else if (id > 801 && id <= 805) {
      return mostlycloudy;
    }
  };
  let list = hoursMain.map((item) => {
    return (
      <RepeatedDiv
        time1={item.dt_txt.split(" ")[1].split(":")[0]}
        degree1={item.main.temp}
        src={liveImages(item.weather[0].id)}
      />
    );
  });
  return <div className="footer">{list}</div>;
};

export default Footer;



// import React from "react";
// import RepeatedDiv from "./RepeatedDiv";
// import FakeWeather from "../fakeWeatherData.json";

// import storm from "../img/weather-icons/storm.svg";
// import clear from "../img/weather-icons/clear.svg";
// import drizzle from "../img/weather-icons/drizzle.svg";
// import fog from "../img/weather-icons/fog.svg";
// import mostlycloudy from "../img/weather-icons/mostlycloudy.svg";
// import partlycloudy from "../img/weather-icons/partlycloudy.svg";
// import rain from "../img/weather-icons/rain.svg";
// import snow from "../img/weather-icons/snow.svg";



// const Footer = ({ hoursFooter }) => {

//   // const icon = (id) => {
//   //   if (id <= 300) return storm;
//   //   else if (id > 300 && id <= 500) return drizzle;
//   // };

//   const Down = ({ hoursFooter }) => {
//     const liveImages = (id, flag) => {
//       if (id <= 300) {
//         return storm;
//       } else if (id > 300 && id <= 499) {
//         return drizzle;
//       } else if (id >= 500 && id <= 599) {
//         return rain;
//       } else if (id >= 600 && id <= 699) {
//         return snow;
//       } else if (id >= 700 && id <= 799) {
//         return fog;
//       } else if (id == 800) {
//         return clear;
//       } else if (id == 801) {
//         return partlycloudy;
//       } else if (id > 801 && id <= 805) {
//         return mostlycloudy;
//       }
//     };
//   let list = hoursFooter.map((item) => {
//     return (
//       <RepeatedDiv
//         time1={item.dt_txt.split(" ")[1].split(":")[0]}
//         degree1={item.main.temp}
//       // src={icon(item.weather[0].id)}
//       />
//     );
//   });
//   // console.log(FakeWeather);
//   return (
//     <div id="footer">
//       {list}

//     </div>
//   );
// };
// }
// export default Footer;








// // import React from "react";

// // const Footer = (props) => {
// //     return (

// //         <div className="final-section">
// //         <p>{props.clock}</p>
// //         <img src= {props.image} width="150px"></img>
// //         <p className="degree-number">{ props.degree}°</p>
// //       </div>

// //     );
// //   };
// //   export default Footer